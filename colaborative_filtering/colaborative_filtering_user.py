import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import pairwise_distances
import sys
from pymongo import MongoClient


#Load data from mongoDB
client = MongoClient("mongodb://localhost:27017")
db = client.movielens
df  = pd.DataFrame(list(db.ratings.find()))

#Server environment
if (len(sys.argv) > 1):
    new_user_ratings = {}
    for rating in sys.argv[1].split(","):
        new_user_ratings[int(rating.split(":")[0])] = int(rating.split(":")[1])
#Local environment
else:
    new_user_ratings = {56:5,96:5,17:4,226:5,222:3,313:2,590:4}


#Number of users in data set
n_users = df.user_id.unique().shape[0]
#Number of items in data set
n_items = df.item_id.unique().shape[0]

#Matrix with train data
train_data_matrix = np.zeros((n_users+1, n_items))
for row in df.itertuples(index=True, name='Pandas'):
    train_data_matrix[int(getattr(row, "user_id"))-1, int(getattr(row, "item_id"))-1] = int(getattr(row, "rating"))
#Last row in matrix represents current user
for i in new_user_ratings.keys():
    train_data_matrix[944 - 1, i - 1] = new_user_ratings[i]

#Similarity matrix
userSimilarity = pairwise_distances(train_data_matrix, metric='cosine')

##
## Predict ratings user/item based colaborative filtering
## Params:
##
def predictUserBased(ratings, similarity):
    """
    :param ratings:
    :param similarity:
    :return: prediction
    """
    #Mean rating for each user
    mean_user_rating = ratings.mean(axis=1)
    # Difference for each rating user mean rating
    ratings_diff = (ratings - mean_user_rating[:, np.newaxis])
    # Prediction of emty rating
    pred = mean_user_rating[:, np.newaxis] + similarity.dot(ratings_diff) / np.array([np.abs(similarity).sum(axis=1)]).T
    return pred

##
##Find the top-k movie based on the ordered ratings
##
def getTopKIdsByUser(prediction, user_id, k=5):
    """
    :param prediction:
    :param user_id:
    :param k:
    :return: top_k_movies
    """
    return [x+1 for x in np.argsort(prediction[user_id,np.where(train_data_matrix[user_id, :] == 0)[0]])[:-k-1:-1]]


userPrediction = predictUserBased(train_data_matrix, userSimilarity)
sys.stdout.write(str(getTopKIdsByUser(userPrediction,944-1)))
