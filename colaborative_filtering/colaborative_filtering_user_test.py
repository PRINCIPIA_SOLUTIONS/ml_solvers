import numpy as np
import pandas as pd
from sklearn.metrics.pairwise import pairwise_distances
from sklearn.metrics import mean_squared_error
from sklearn import model_selection
from pymongo import MongoClient
from math import sqrt

#Load data from mongoDB
#client = MongoClient("mongodb://localhost:27017")
#db = client.movielens
#df  = pd.DataFrame(list(db.ratings.find()))

#Load data from csv
header = ['user_id', 'item_id', 'rating', 'timestamp']
df = pd.read_csv('../ml-100k/u.data', sep='\t', names=header)

matrics = ['cityblock', 'cosine', 'euclidean', 'l1', 'l2', 'manhattan']

#Number of users in data set
n_users = df.user_id.unique().shape[0]
#Number of items in data set
n_items = df.item_id.unique().shape[0]

#Splits dataset into two datasets, one for training, ono for testing
train_data, test_data = model_selection.train_test_split(df, test_size=0.25)

#Matrix with train data
train_data_matrix = np.zeros((n_users, n_items))
for row in train_data.itertuples(index=True, name='Pandas'):
    train_data_matrix[int(getattr(row, "user_id"))-1, int(getattr(row, "item_id"))-1] = int(getattr(row, "rating"))
#Matrix with test data
test_data_matrix = np.zeros((n_users, n_items))
for row in test_data.itertuples(index=True, name='Pandas'):
    test_data_matrix[int(getattr(row, "user_id"))-1, int(getattr(row, "item_id"))-1] = int(getattr(row, "rating"))


##
## Predict ratings user/item based colaborative filtering
## Params:
##
def predictUserBased(ratings, similarity):
    """
    :param ratings:
    :param similarity:
    :return: prediction
    # """
    #Mean rating for each user
    mean_user_rating = ratings.mean(axis=1)
    # Difference for each rating user mean rating
    ratings_diff = (ratings - mean_user_rating[:, np.newaxis])
    # Prediction of emty rating
    pred = mean_user_rating[:, np.newaxis] + similarity.dot(ratings_diff) / np.array([np.abs(similarity).sum(axis=1)]).T
    return pred


##
## Calculate RMSE between two datasets
##
def rmse(prediction, ground_truth):
    '''
    :param prediction:
    :param ground_truth:
    :return:
    '''
    prediction = prediction[ground_truth.nonzero()].flatten()
    ground_truth = ground_truth[ground_truth.nonzero()].flatten()
    return sqrt(mean_squared_error(prediction, ground_truth))


outputMask = "MATRIC: {0:10} RMSE: {1:.2f}"
for matric in matrics:
    userSimilarity = pairwise_distances(train_data_matrix, metric=matric)
    userPrediction = predictUserBased(train_data_matrix, userSimilarity)
    absoluteUserPrediction = userPrediction + train_data_matrix.mean(axis=1)[:, np.newaxis]
    print(outputMask.format(matric,rmse(absoluteUserPrediction, test_data_matrix)))