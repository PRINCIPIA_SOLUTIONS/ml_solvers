from pymongo import MongoClient
import pandas as pd

client = MongoClient("mongodb://localhost:27017")
db = client.movielens

db.movies.delete_many({})
db.ratings.delete_many({})

df_item_header = ['movie id' , 'movie title' , 'release date' , 'video release date',
              'IMDb URL' , 'unknown' , 'Action' , 'Adventure' , 'Animation' ,
              'Childrens' , 'Comedy' , 'Crime' , 'Documentary' , 'Drama' , 'Fantasy' ,
              'Film-Noir' , 'Horror' , 'Musical' , 'Mystery' , 'Romance' , 'Sci-Fi' ,
              'Thriller' , 'War' , 'Western' ]
df_item = pd.read_csv("ml-100k/u.item", sep = '|', dtype=str, encoding = "ISO-8859-1",names=df_item_header)




for index, row in df_item.iterrows():
    item = {}
    item['item_id']=row['movie id']
    item['item_name'] = row['movie title']
    item['item_url'] = row['IMDb URL']
    db.movies.insert_one(item)

for movie in db.movies.find():
     print(movie)

###

df_rating_header = ['user_id' , 'item_id' , 'rating' , 'timestamp' ]
df_rating = pd.read_csv("ml-100k/u.data", delim_whitespace=True, dtype=str, encoding = "ISO-8859-1",names=df_rating_header)



for index, row in df_rating.iterrows():
    rating = {}
    rating['user_id']=row['user_id']
    rating['item_id'] = row['item_id']
    rating['rating'] = row['rating']
    db.ratings.insert_one(rating)

for rating in db.ratings.find():
    print(rating)